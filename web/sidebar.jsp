<!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">                
                  <li class="active">
                      <a class="" href="home.jsp">
                          <i class="icon_house_alt"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a class="" href="Attendance.jsp">
                          <i class="icon_house_alt"></i>
                          <span>Attendance</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="icon_document_alt"></i>
                          <span>Leaves</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                      <ul class="sub">
                          <li><a class="" href="leaves.jsp">Apply Leave</a></li>                          
                          <li><a class="" href="leave_status.jsp">Status</a></li>
                      </ul>
                  </li>       
                  <li class="sub-menu">
                      <a href="score.jsp;" class="">
                          <i class="icon_desktop"></i>
                          <span>Scores</span>
                          
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a class="" href="resourceDownload.jsp">
                          <i class="icon_house_alt"></i>
                          <span>Resources</span>
                      </a>
                  </li>    
                             
                  <li class="sub-menu">
                      <a class="" href="Timetable.jsp">
                          <i class="icon_house_alt"></i>
                          <span>Timetable</span>
                      </a>
                  </li>
                  
                  <li class="sub-menu">
                      <a class="" href="feedback.jsp">
                          <i class="icon_documents_alt"></i>
                          <span>Feedback</span>
                      </a>
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      