<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

<%
  String id = (String)session.getAttribute("userid");
  String desig2 = (String)session.getAttribute("desig");

try {
 Class.forName("com.mysql.jdbc.Driver");
} catch (ClassNotFoundException e) {
e.printStackTrace();
}

%>
<%@ include file="header.jsp" %>
<% if (desig2.equals("Student"))
        { %>
            <%@ include file="sidebar.jsp" %>
        <%    
        }else 
        { %>
        <%@ include file="teacher_sidebar.jsp" %>
<%
        }
try{ 
  Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/javadb",
            "root", "");
Statement st = con.createStatement();
ResultSet rs;
    rs = st.executeQuery("select * from masterdb where id = '" + id + "'");
   
while(rs.next()){
    
%>
<!--main content start-->
        <section id="main-content">
            <section class="wrapper">            
                <!--overview start-->
                
                    <div class="col-lg-12">
                            <h3 class="page-header"><i class="fa fa-laptop"></i>Profile</h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="col-sm-5 col-sm-offset-1">
                                        <form class="form-validate form-horizontal" id="info_form" method="get" action="">
                                            <div class="form-group">
                                                <label for="usr">Name:</label>
                                                <input type="text" class="form-control" name="fname" value="<%=rs.getString("fname") %> <%=rs.getString("mname") %> <%=rs.getString("lname") %>" required/>
                                            </div> 
                                            <div class="form-group">
                                                <label for="usr">Email:</label>
                                                <input type="email" class="form-control" required name="email" value="<%=rs.getString("email") %>" />
                                            </div>
                                            <div class="form-group">
                                                <label for="usr">Date of Birth:</label>
                                                <input type="text" class="form-control" name="dob" value="<%=rs.getString("dob") %>"/>
                                            </div>
                                            <label for="usr">Gender:</label>  
                                            <div class="form-group">
                                                <input type="text" class="form-control" required name="gender" value="<%=rs.getString("gender") %>" />
                                            </div>
                                            <div class="form-group">
                                                <label for="sel1">Department:</label>
                                                <input type="text" class="form-control" name="dob" value="<%=rs.getString("department") %>"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="sel1">Class:</label>
                                                <input type="text" class="form-control" name="dob" value="<%=rs.getString("class") %>"/>
                                            </div>  
                                            <div class="form-group">
                                                <label for="sel1">Batch:</label>
                                                <input type="text" class="form-control" name="dob" value="<%=rs.getString("batch") %>"/>
                                           </div>
                                            <div class="form-group">
                                                <label for="usr">Phone Number:</label>
                                                <input type="text" class="form-control" name="phno" value="<%=rs.getString("ph_no") %>" min="7000000000" max="9999999999" required/>
                                            </div>
                                            <div class="form-group">
                                                <label for="sel1">Nationality:</label>
                                                <input type="text" class="form-control" name="dob" value="<%=rs.getString("nationality") %>"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="usr">Father's Name:</label>
                                                <input type="text" class="form-control" name="dob" value="<%=rs.getString("father") %>"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="usr">Mother's Name:</label>
                                                <input type="text" class="form-control" name="dob" value="<%=rs.getString("mother") %>"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="usr">Address:</label>
                                                <textarea rows="3" class="form-control" columns="5" name="add"><%=rs.getString("address") %></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="usr">City:</label>
                                                <input type="text" class="form-control" name="city" value="<%=rs.getString("city") %>" />
                                            </div> 
                                            <div class="form-group">
                                                <label for="usr">State:</label>
                                                <input type="text" class="form-control" name="state" value="<%=rs.getString("state") %>" />
                                            </div>
                                            <div class="form-group">
                                                <label for="usr">Pin Code:</label>
                                                <input type="text" class="form-control" name="pin" value="<%=rs.getString("pincode") %>"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="sel1">Blood Group:</label>
                                                <input type="text" class="form-control" name="pin" value="<%=rs.getString("bloodgroup") %>"/>
                                            </div>     
                                                                            </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                
            </section>
        </section>
    <!--main content end-->
<% 
}

} catch (Exception e) {
e.printStackTrace();
}
%>
<%@ include file="footer.jsp" %>