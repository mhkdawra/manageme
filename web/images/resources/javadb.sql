-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2017 at 10:27 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `javadb`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `class` varchar(30) NOT NULL,
  `batch` varchar(7) NOT NULL,
  `month` varchar(30) NOT NULL,
  `image` varchar(80) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `class`, `batch`, `month`, `image`) VALUES
(1, 'MCA 3', 'E', 'April', 'prog1.cpp');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `t_id` varchar(15) NOT NULL,
  `s_id` varchar(15) NOT NULL,
  `comment` varchar(300) NOT NULL,
  `class` varchar(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`t_id`, `s_id`, `comment`, `class`) VALUES
('Anuj Sharma', 'mhk', 'very good', 'MCA 1'),
('AnujSharma', 'mhk', 'good', 'MCA 2'),
('Sonal Chawla', 'mhk', 'very good', 'MCA 1'),
('Indu Chhabra', 'mhk', 'good', 'MCA 1'),
('Anu Gupta', 'mhk', 'Very Good', 'MCA 2');

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `leave_id` int(11) NOT NULL,
  `id` varchar(15) NOT NULL,
  `leave_type` varchar(15) NOT NULL,
  `comment1` varchar(200) DEFAULT NULL,
  `from_date` varchar(12) NOT NULL,
  `to_date` varchar(12) NOT NULL,
  `status1` varchar(30) NOT NULL,
  `image_url` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leaves`
--

INSERT INTO `leaves` (`leave_id`, `id`, `leave_type`, `comment1`, `from_date`, `to_date`, `status1`, `image_url`) VALUES
(1, 'mhk', 'Medical', ' ,, ,', '2017-04-21', '2017-04-25', 'Approved', NULL),
(2, 'mhk', 'medical', 'abcd', '2017-04-08', '2017-04-08', 'Approved', 'murphu.jpg'),
(3, 'mhk', 'Duty', ' ,n,m,n', '2017-04-21', '2017-04-25', 'Approved', NULL),
(4, 'mhk', 'Duty', ' addnlkddl', '2017-04-21', '2017-04-25', 'Approved', NULL),
(5, 'mhk', 'Medical', ' try try try', '2017-04-21', '2017-04-25', 'Approved', 'New Text Document.txt'),
(6, 'mhk', 'Other', 'poo poo ', '2017-08-09', '2017-08-11', 'Approved', 'murphu.jpg'),
(7, 'mhk', 'Other', 'Hello Karishma ', '2017-04-21', '2017-04-25', 'Approved', 'a.txt'),
(8, 'mhk', 'Medical', 'abcdegfgh', '2017-08-09', '2017-08-10', 'Approved', 'murphu.jpg'),
(11, 'mhk', 'Medical', ' dfghjkl;', '2017-04-21', '2017-04-25', 'Not Approved', 'average.cpp'),
(12, 'mhk', 'Duty', 'nffsn', '2017-04-21', '2017-04-28', 'Not Approved', ''),
(13, 'mhk', 'Medical', ' dsfghjkl', '2017-04-21', '2017-04-28', 'Approved', 'IMG_1453967997834-1962993818_1453968004828.jpg'),
(14, 'sukhpreet', 'Duty', 'Cannot come. Have to go to Delhi.', '2017-04-21', '2018-06-05', 'Approved', 'post.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` varchar(15) DEFAULT NULL,
  `pass` varchar(30) DEFAULT NULL,
  `desig` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `pass`, `desig`) VALUES
('mhk', 'abcd', 'Student'),
('yasd', 'abcd', 'Student'),
('hji', 'hgi', 'Student'),
('abcd', 'abcd', 'st'),
('anuj', 'sharma', 'Teacher'),
('anuj', 'sharma', 'Teacher'),
('yashda', 'yashda', 'Student'),
('a_gupta', 'anugupta', 'Teacher'),
('karishma', 'tirthani', 'Student'),
('sukhpreet', 'singh', 'Student');

-- --------------------------------------------------------

--
-- Table structure for table `masterdb`
--

CREATE TABLE `masterdb` (
  `id` varchar(15) NOT NULL,
  `desig` varchar(20) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `mname` varchar(20) DEFAULT NULL,
  `lname` varchar(20) DEFAULT NULL,
  `dob` date NOT NULL,
  `gender` varchar(1) NOT NULL,
  `department` varchar(50) DEFAULT NULL,
  `class` varchar(6) NOT NULL,
  `batch` varchar(7) NOT NULL,
  `ph_no` bigint(20) NOT NULL,
  `nationality` varchar(30) DEFAULT NULL,
  `father` varchar(30) DEFAULT NULL,
  `mother` varchar(30) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `bloodgroup` varchar(3) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masterdb`
--

INSERT INTO `masterdb` (`id`, `desig`, `fname`, `mname`, `lname`, `dob`, `gender`, `department`, `class`, `batch`, `ph_no`, `nationality`, `father`, `mother`, `address`, `city`, `state`, `pincode`, `email`, `bloodgroup`) VALUES
('mhk', 'Student', 'mehak', '', 'dawra', '2013-04-19', 'F', 'dcsa', '', '', 9888812101, 'indian', 'abcd', 'edgh', 'akdakjbd', 'vjhvjhvj', 'vjhvjhv', 140603, 'kbkjavdkvfdkvfk@.com', 'o+'),
('abcd', 'st', 'bbbjhb', '', 'bhbjhb', '1994-02-06', 'f', 'iuybub', '', '', 45874, 'bbjhb', ' jh', 'jbjh', 'bjbj', 'bjhb', 'bb', 1414, ' jhbjv ', 'o'),
('anuj', 'Teacher', 'Anuj', '', 'Sharma', '1972-06-08', 'M', 'DCSA', '', '', 9876543210, 'Indian', 'ABCDE', 'FGHIJ', 'sfkjsbfkb', 'Chandigarh', 'ChandÄ«garh', 140603, 'anujsharma@abcd.com', 'O+'),
('yashda', 'Student', 'Yashda', '', 'Singla', '1994-06-02', 'F', 'DCSA', 'MCA 2', 'Morning', 8283889042, 'null', 'Rajinder Singla', 'Rajni Singla', 'Room no. 8-A girls hostel no. 1\r\nPanjab University, Chandigarh', 'Chandigarh', 'Chand&#299;garh', 160014, 'yashda@gmail.com', 'A+'),
('a_gupta', 'Teacher', 'Anu', '', 'Gupta', '1979-05-09', 'F', 'DCSA', 'none', 'none', 9874563210, 'null', 'Abcd', 'Efgh', 'Chandigarh', 'Chandigarh', 'Chand&#299;garh', 160014, 'anu@gmail.com', 'AB+'),
('karishma', 'Student', 'Karishma', '', 'TIrthani', '1994-01-25', 'F', 'DCSA', 'MCA 2', 'Morning', 8054865607, 'null', 'Vijay', 'Vandana', 'Sector 15', 'Panchkula', 'Haryana', 134113, 'karishma@gmail.com', 'O+'),
('sukhpreet', 'Student', 'Sukhpreet', '', 'Singh', '1991-06-19', 'M', 'DCSA', 'MCA 2', 'Morning', 7814704405, 'null', 'Kamaldeep Singh', 'Sarvjeet Kaur', 'Una', 'Una', 'HP', 174306, 'sukhpreetsingh@gmail.com', 'A+');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `t_id` varchar(6) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `class` varchar(6) NOT NULL,
  `message` varchar(300) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`t_id`, `fname`, `lname`, `class`, `message`, `time`) VALUES
('anuj', 'Anuj', 'Sharma', 'MCA 3', 'fafsfsgfssg', '2017-04-29 00:21:36'),
('anuj', 'Anuj', 'Sharma', 'MCA 2', 'class at 10 tomorrow', '2017-04-29 08:47:24'),
('anuj', 'Anuj', 'Sharma', 'none', 'hello world\r\n', '2017-04-29 09:31:56'),
('anuj', 'Anuj', 'Sharma', 'MCA 2', 'vvjvjvjhv', '2017-04-29 09:51:05'),
('anuj', 'Anuj', 'Sharma', 'null', 'Class at 10 tomorrow. Class at 10 tomorrow.Class at 10 tomorrow.Class at 10 tomorrow.Class at 10 tomorrow.Class at 10 tomorrow.Class at 10 tomorrow.', '2017-05-01 15:34:41');

-- --------------------------------------------------------

--
-- Table structure for table `resource`
--

CREATE TABLE `resource` (
  `id` int(11) NOT NULL,
  `t_id` varchar(15) NOT NULL,
  `resources` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resource`
--

INSERT INTO `resource` (`id`, `t_id`, `resources`) VALUES
(1, 'anuj', 'murphu.jpg'),
(2, 'anuj', 'oddeven.cpp'),
(3, 'anuj', 'IMG_1453967997834-1962993818_1453968004828.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `score`
--

CREATE TABLE `score` (
  `class` varchar(20) NOT NULL,
  `batch` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `file` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `score`
--

INSERT INTO `score` (`class`, `batch`, `type`, `file`) VALUES
('MSC-IT 1', 'E', 'i2', 'oddeven.cpp');

-- --------------------------------------------------------

--
-- Table structure for table `timetable`
--

CREATE TABLE `timetable` (
  `id` int(10) NOT NULL,
  `class` varchar(30) NOT NULL,
  `batch` varchar(30) NOT NULL,
  `month` varchar(30) NOT NULL,
  `image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timetable`
--

INSERT INTO `timetable` (`id`, `class`, `batch`, `month`, `image`) VALUES
(1, 'MCA 1', 'M', '', 'think.jpg'),
(2, 'MCA 2', 'M', '', 'think.jpg'),
(3, 'MCA 3', 'M', '', 'think.jpg'),
(4, 'MCA 1', 'E', '', 'think.jpg'),
(5, 'MCA 2', 'E', '', 'think.jpg'),
(6, 'MCA 3', 'E', '', 'think.jpg'),
(7, 'MSC-IT 1', 'NONE', '', 'think.jpg'),
(8, 'MSC-IT 2', 'NONE', '', 'think.jpg'),
(9, 'MCA 2', 'M', 'April', 'murphu.jpg'),
(10, 'MCA 3', 'M', 'January', 'murphu.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`t_id`,`s_id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`leave_id`);

--
-- Indexes for table `masterdb`
--
ALTER TABLE `masterdb`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ph_no` (`ph_no`);

--
-- Indexes for table `resource`
--
ALTER TABLE `resource`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `score`
--
ALTER TABLE `score`
  ADD PRIMARY KEY (`class`,`batch`,`type`);

--
-- Indexes for table `timetable`
--
ALTER TABLE `timetable`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `leave_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `resource`
--
ALTER TABLE `resource`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `timetable`
--
ALTER TABLE `timetable`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
