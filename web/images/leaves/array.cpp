#include<stdio.h>
#include<stdlib.h>
int main()
{
	int a[30];
	int i,j,size,ch,ipos,dpos,ele,max,min,temp;
	char choice;

	printf("Enter the size of the array:\n");
	scanf("%d",&size);
	printf("Enter the elements of array:\n");
	for(i=0;i<size;i++)
	{
		scanf("%d",&a[i]);
	}
	do
	{
		printf("\nArray Operations: \n");
		printf("1.Traverse the entered array(print)\n");
		printf("2.Insertion at specific positions\n");
		printf("3.Deletion from specific positions\n");
		printf("4.Find Maximum and Minimum elements of the array\n");
		printf("5.Reverse an array");
		printf("6.Exit\n");
		printf("Which operation would you like to perform? \n");
		scanf("%d",&ch);
		switch(ch)
		{
			case 1:
				printf("\nTraversing the array entered by user...");
				for(i=0;i<size;i++)
				{
					printf("%d \t",a[i]);
				}
				printf("\nArray successully traversed\n");
				
			case 2:
			printf("Enter the position to insert at:\n");
			scanf("%d",&ipos);
			printf("Enter the element to insert:\n");
			scanf("%d",&ele);
			for(i=size-1;i>=ipos-1;i--)
			a[i+1]=a[i];
			a[ipos-1]=ele;
			printf("Array after insertion:\n");
			for(i=0;i<=size;i++)
			{
				printf("%d ",a[i]);
			}
			break;
		case 3:printf("Enter the position of array to be deleted:\n");
		scanf("%d",&dpos);
		for(i=dpos-1;i<size;i++)
			a[i]=a[i+1];
			--size;
		printf("Array after deletion:\n");
		for(i=0;i<size;i++)
			printf("%d ",a[i]);
		break;
		
		case 4:
			max=a[0];
	for(i=1;i<size;i++)
	{
		if(max<a[i])
		max=a[i];
	}
	min=a[0];
	for(i=1;i<size;i++)
	{
		if(min>a[i])
		min=a[i];
	}
	printf("\nThe maximum element of the entered array is %d\n",max);
	printf("\nThe minimum elemt of the entered array is %d",min);
	break;
	
		case 5:
			j = i - 1;   // j will Point to last Element
   			i = 0;       // i will be pointing to first element
 
  			 while (i < j) {
     		temp = a[i];
      a[i] = a[j];
      a[j] = temp;
      i++;
      j--;
   }
 
   printf("\n===============\n");
   printf("\nResult after reversal : ");
   for (i = 0; i<size; i++) {
      printf("%d \t", a[i]);
   }
		
		case 6: exit(1);
		default:printf("Wrong Choice...\n");
	}
	printf("Would you like to continue? Y/N \n");
	scanf("%s",&choice);
}while(choice=='Y' || choice=='y');
return 0;
}
