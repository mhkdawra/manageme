#include<iostream>
#include<string.h>
#include<iomanip>
using namespace std;
class books
{
	char title[60],author[30],publisher[30];
	int stock;
	double price;
	int update(int);
	static int count,count1;
	public:
		books();
		void input_data();
		int search(char[],char[]);
		int print_cost(int);
		void display();
		void transaction();
		void up_price(int);
};
int books::count;
int books::count1;
books::books()
{
	char *title=new char[60];
	char *author=new char[30];
    char *publisher=new char[30];
    price=0;
    stock=0;
}
void books::input_data()
{
	cout<<"Enter the details of your book"<<endl;
	cout<<setw(10)<<"Title:";
	cin.ignore();
	cin.getline(title,60);
	cout<<endl;
	cout<<setw(10)<<"Author:";
	cin.getline(author,30);
	cout<<endl;
	cout<<setw(10)<<"Publisher:";
	cin.getline(publisher,30);
	cout<<endl;
	cout<<setw(10)<<"Price:";
	cin>>price;
	cout<<endl;
	cout<<setw(10)<<"stock:";
	cin>>stock;
	cout<<endl;
}
void books::display()
{
	cout<<endl;
	cout<<title<<"\t"<<author<<"\t"<<publisher<<"\t\t"<<price<<"\t"<<stock;
}
int books::search(char Title[],char Author[])
{
	if(strcmp(title,Title)&&(strcmp(author,Author)))
	{
		return 0;
	}
	else
	{
		return 1;
		count1++;
	}
}
int books::print_cost(int copies)
{
	if(copies<=stock)
	{
		cout<<"Entered copies available"<<endl;
		cout<<"Total price for "<<copies<<" : "<<(price*copies);
		stock=stock-copies;
		count++;
	}
	else
	{
		cout<<"Not in stock"<<endl;
		count1++;
	}
	return 0;
}
int books::update(int k)
{
	price=k;
	return k;	
}
void books::up_price(int n)
{
	int new_p;
	if(n<0)
	{
		cout<<"You have entered the wrong price"<<endl;
	}
    new_p=update(n);
    cout<<endl<<"Updated price of the book is:"<<new_p;
}
void books::transaction()
{
	int ch1;
	cout<<endl<<"Transactions"<<endl;
	cout<<"1.Print successful transactions"<<endl;
	cout<<"2.Print unsuccessful transactions"<<endl;
	cout<<"enter choice: "<<endl;
	cin>>ch1;
	switch(ch1)
	{
		case 1:
			cout<<"Successful transactions are"<<count;
			break;
		case 2:
			cout<<"Unsuccessful transactions are"<<count1;
			break;
		default:
			cout<<"wrong choice"<<endl;
			break;
	}
}
int main()
{
		int num,copy,ch,i,flag,book_no,pric;
	char search_t[60],search_a[30],choice;
	books book[50],boo;
	do
	{
	cout<<"~_~_~_~ BOOK STORE ~_~_~_~"<<endl;
	cout<<"Menu options"<<endl;
	cout<<"1. Insert details"<<endl;
	cout<<"2. Display books"<<endl;
	cout<<"3. buy a book"<<endl;
	cout<<"4. Update price"<<endl;
	cout<<"5. Transactions"<<endl;
	cout<<"6. Exit"<<endl;
	cout<<"Enter your choice: ";
	cin>>ch;
	switch(ch)
	{
		default:
			cout<<"Wrong Choice...";
			break;
		case 1:
			cout<<"How many books would you like to insert?";
			cin>>num;
			for(i=0;i<num;i++)
			{
				book[i].input_data();
			}
			cout<<"Data inserted successfully"<<endl;
			break;
		case 2:
			cout<<"\n"<<"TITLE"<<"\t"<<"AUTHOR"<<"\t"<<"PUBLISHER"<<"\t"<<"PRICE"<<"\t"<<"STOCK";
            for(i=0;i<num;i++)
    		{
            	cout<<"\n";
				book[i].display();   
    		}
			break;
		case 3:
			cout<<"Enter the title of the book to be searched:  ";
			cin.ignore();
			cin.getline(search_t,60);
			cout<<"Enter the author of the book to be searched:  ";
			cin.getline(search_a,30);
			for(i=0;i<num;i++)
            {
				if(book[i].search(search_t,search_a))
                {
                   flag=1;
				   cout<<"\n"<<"TITLE"<<"\t"<<"AUTHOR"<<"\t"<<"PUBLISHER"<<"\t"<<"PRICE"<<"\t"<<"STOCK";
                   book[i].display();
                   book_no=i;
            	}
        	}
        	if(flag==1)
        	{
        		cout<<endl<<"available"<<endl;
        		cout<<"Enter the number of copies required:  ";
        		cin>>copy;
        		book[book_no].print_cost(copy);
        	}
        	else
        	{
        		cout<<"not available";
        	}
        	break;
        case 4:
        	cout<<"Enter the title of the book to be updated:  ";
			cin.ignore();
			cin.getline(search_t,60);
			cout<<"Enter the author of the book to be updated:  ";
			cin.getline(search_a,30);
        	for(i=0;i<num;i++)
            {
				if(book[i].search(search_t,search_a))
                {
                   flag=1;
				   cout<<"\n"<<"TITLE"<<"\t"<<"AUTHOR"<<"\t"<<"PUBLISHER"<<"\t"<<"PRICE"<<"\t"<<"STOCK";
                   book[i].display();
                   book_no=i;
            	}
        	}
        	if(flag==1)
        	{
        		cout<<endl<<"Required book exists"<<endl;
        		cout<<"Enter the new price:  ";
        		cin>>pric;
        		book[book_no].up_price(pric);
        	}
        	else
        	{
        		cout<<"Required book is not available";
        	}
        	break;
        case 5:
        	boo.transaction();
        	break;
		case 6:
			cout<<endl<<"Finishing up..."<<endl;
			return 0;
			break;
	}
	cout<<"\nWould you like to continue? Y/N :";
	cin>>choice;
	}while(choice=='y'||choice=='Y');
}
