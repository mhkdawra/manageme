//Program to calculate the average marks of n students
//using class and scope resolution operator
#include<iostream>
using namespace std;
class student
{
	private:
		int marks;
	public:
		void display();
}
void student::display()
{
	int n,sum=0,average;
	int s[10];
	cout<<"Enter the number of students"<<endl;
	cin>>n;
	cout<<"enter the marks for each student"<<endl;
	for(int i=0;i<n;i++)
	{
		cin>>s[i].marks;
		sum=sum+s[i].marks;
	}
	average=sum/n;
	cout<<"Average marks of"<<n<<"students is:"<<average<<endl;
}
int main()
{
	student stud;
	stud.display();
}
