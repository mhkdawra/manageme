//Program to add,suvtract, multiply and divide two numbers
//using class and scope resolution operator
#include<iostream>
using namespace std;
class operations
{
		int num1,num2;
	public:
		int sum(int,int);
		int sub(int,int);
		int mul(int,int);
		int div(int,int);
};
int operations::sum(int a,int b)
{
	int add;
	add=a+b;
	return add;
}
int operations::sub(int a,int b)
{
	int result;
	result=a-b;
	return result;
}
int operations::mul(int a,int b)
{
	int product;
	product=a*b;
	return product;
}
int operations::div(int a,int b)
{
	int quotient;
	quotient=a/b;
	return quotient;
}
int main()
{
	cout<<"Welcome to The Calculator"<<endl;
	operations op;
	int int1,int2;
	cout<<"Enter two integers"<<endl;
	cin>>int1>>int2;
	cout<<"Operations performed on "<<int1<<" and "<<int2<<" are: "<<endl;
	cout<<"Addition = "<<op.sum(int1,int2)<<endl;
	cout<<"Subtraction = "<<op.sub(int1,int2)<<endl;
	cout<<"Multiplication = "<<op.mul(int1,int2)<<endl;
	cout<<"Division = "<<op.div(int1,int2)<<endl;
}
