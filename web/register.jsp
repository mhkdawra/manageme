<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Creative - Bootstrap Admin Template</title>

    <!-- Bootstrap CSS -->    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="css/elegant-icons-style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
	<link href="css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="css/fullcalendar.css">
	<link href="css/widgets.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
	<link href="css/xcharts.min.css" rel=" stylesheet">	
	<link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">
  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">
     
      
      <header class="header dark-bg">
            <!--logo start-->
            <a href="index.html" class="logo">MANAGE <span class="lite">ME</span></a>
            <!--logo end-->
            
      </header>      
      <!--header end-->
      <div class="container register_form" style="padding-top: 100px;padding-bottom: 100px;">
      <form method="post" action="controller_registration.jsp">
        <div class="form-group">
            <label for="sel1">Select Designation:</label>
            <select class="form-control" id="sel1" name="desig">
                <option value="Teacher">Teacher</option>
                
                <option value="Student">Student</option>
            </select>
        </div>
        <div class="form-group">
            <label for="usr">First Name:</label>
            <input type="text" class="form-control" name="fname" value="" required/>
        </div> 
        <div class="form-group">
            <label for="usr">Middle Name:</label>
            <input type="text" class="form-control" name="mname" value="" />
        </div>
        <div class="form-group">
            <label for="usr">Last Name:</label>
            <input type="text" class="form-control" name="lname" value="" required/>
        </div>
        <div class="form-group">
            <label for="usr">Email:</label>
            <input type="email" class="form-control" required name="email" value="" />
        </div> 
        <div class="form-group">
            <label for="usr">User Name:</label>
            <input type="text" class="form-control" name="uname" value="" required/>
        </div>  
        <div class="form-group">
            <label for="usr">Password:</label>
            <input type="password" class="form-control" name="pass" value="" required/>
        </div>
        <div class="form-group">
            <label for="usr">Date of Birth:</label>
            <input type="text" class="form-control" name="dob" placeholder="YYYY-MM-DD" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" title="Enter a date in this formart YYYY-MM-DD"/>
        </div>
        <label for="usr">Gender:</label>  
        <div class="radio">
            
            <label><input type="radio" name="gender" value="M" />Male</label>
        </div>
        <div class="radio">
            
            <label><input type="radio" name="gender" value="F" />Female</label>
        </div>
        <div class="form-group">
            <label for="sel1">Select Department:</label>
            <select class="form-control" id="sel1" name="dep">
                <option value="DCSA">DCSA</option>
                <option value="Physics">Physics</option>
                <option value="Mathematics">Mathematics</option>
            </select>
        </div>
        <div class="form-group">
            <label for="sel1">Select Class:</label>
            <select class="form-control" id="sel1" name="class">
                <option value="none">None</option>
                <option value="MCA 1">MCA 1</option>
                <option value="MCA 2">MCA 2</option>
                <option value="MCA 3">MCA 3</option>
                <option value="MSC 1">MSC 1</option>
                <option value="MSC 2">MSC 2</option>
            </select>
        </div>  
        <div class="form-group">
            <label for="sel1">Select Batch:</label>
            <select class="form-control" id="sel1" name="batch">
                <option value="none">None</option>
                <option value="Morning">Morning</option>
                <option value="Evening">Evening</option>
           </select>
       </div>
        <div class="form-group">
            <label for="usr">Phone Number:</label>
            <input type="text" class="form-control" name="phno" value="" min="7000000000" max="9999999999" required/>
        </div>
        <div class="form-group">
            <label for="sel1">Nationality:</label>
            <select class="form-control" id="sel1" name="batch">
                <option value="Indian">Indian</option>
                <option value="Other">Other</option>
           </select>
        </div>
        <div class="form-group">
            <label for="usr">Father's Name:</label>
            <input type="text" class="form-control" name="father" value="" />
        </div>
        <div class="form-group">
            <label for="usr">Mother's Name:</label>
            <input type="text" class="form-control" name="mother" value="" />
        </div>
        <div class="form-group">
            <label for="usr">Address:</label>
            <textarea rows="3" class="form-control" columns="5" name="add" value=""></textarea>
        </div>
        <div class="form-group">
            <label for="usr">City:</label>
            <input type="text" class="form-control" name="city" value="" />
        </div> 
        <div class="form-group">
            <label for="usr">State:</label>
            <input type="text" class="form-control" name="state" value="" />
        </div>
        <div class="form-group">
            <label for="usr">Pin Code:</label>
            <input type="text" class="form-control" name="pin" value="" min="000000" max="999999"/>
        </div>
        <div class="form-group">
            <label for="sel1">Blood Group:</label>
            <select class="form-control" id="sel1" name="bg">
                <option value="A+">A+</option>
                <option value="A-">A-</option>
                <option value="B+">B+</option>
                <option value="B-">B-</option>
                <option value="O+">O+</option>
                <option value="O-">O-</option>
                <option value="AB+">AB+</option>
                <option value="AB-">AB-</option>
           </select>
        </div> 
                    
                        <input type="submit" class="btn" value="Submit" />
                        <input type="reset" class="btn" value="Reset" />
            </center>
        </form>
      </div>
  </section>
 <%@ include file="footer.jsp" %> 
