<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

<%
  
try {
 Class.forName("com.mysql.jdbc.Driver");
} catch (ClassNotFoundException e) {
e.printStackTrace();
}

%>
<%@ include file="header.jsp" %>
<%@ include file="teacher_sidebar.jsp" %>
    <!--main content start-->
        <section id="main-content">
            <section class="wrapper">            
                <!--overview start-->
                
                    <div class="col-lg-12">
                            <h3 class="page-header"><i class="fa fa-laptop"></i>Approve Leaves</h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <form class="form-validate form-horizontal" id="info_form" method="get" action="controller_approve.jsp">
                                        <table class="table table-striped">
                                            <thead>
                                              <tr>
                                                <th>Leave ID</th>
                                                <th>Student</th>
                                                <th>Leave Type</th>
                                                <th>Comment</th>
                                                <th>From date</th>
                                                <th>To date</th>
                                                <th>View Leave</th>
                                                <th>Approve</th>
                                             
                                                
                                              </tr>
                                            </thead>
                                            
                                        <%
                                            
                                        try{ 
                                          
                                          Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/javadb",
                                                    "root", "");
                                        Statement st = con.createStatement();
                                        Statement st1 = con.createStatement();
                                        ResultSet rs,rs1;
                                            rs= st.executeQuery("select * from leaves where status1='Not Approved'");
                                             while(rs.next()){ 
                                                 String img=rs.getString("image_url");
                                                 String id=rs.getString("id");
                                                 rs1=st1.executeQuery("select fname,lname from masterdb where id='" + id +"'");
                                                
                                                 rs1.next();
                                                
                                       %>
                                       
                                        <tbody>
                                              <tr>
                                                <td><%=rs.getString("leave_id") %></td>
                                                <td><%=rs1.getString("fname")%> <%=rs1.getString("lname")%></td>
                                                <td><%=rs.getString("leave_type") %></td>
                                                <td><%=rs.getString("comment1") %></td>
                                                <td><%=rs.getString("from_date") %></td>
                                                <td><%=rs.getString("to_date") %></td>
                                                                                              
                                                <%
                                                if(img != null && !img.isEmpty()) {
                                                %>
                                                <td><a href="http://localhost:8080/jsp_project/images/leaves/<%=img%>"><%=img%></a></td>
                                                <% }
                                                else{
                                                %>
                                                <td>No Image</td>
                                                <% }
                                                %>
                                                <td><input type="checkbox" name="stat" value="<%=rs.getString("leave_id") %>"></td>
                                              </tr>
                                            

                                        <% 
                                        }

                                        } catch (Exception e) {
                                        e.printStackTrace();
                                        }
                                        %>
                                        </tbody>
                                        </table>
                                        <input type="submit" id="thisField" name="inputName" value="Approve Leaves">
                                    </form>
                                </div>
                            </section>
                        </div>
                    </div>
                
            </section>
        </section>
    <!--main content end-->
 <%@ include file="footer.jsp" %> 
