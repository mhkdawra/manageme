<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

<%
  String id = (String)session.getAttribute("userid");

try {
 Class.forName("com.mysql.jdbc.Driver");
} catch (ClassNotFoundException e) {
e.printStackTrace();
}
%>
<%@ include file="header.jsp" %>
<%@ include file="sidebar.jsp" %>
    <!--main content start-->
        <section id="main-content">
            <section class="wrapper">            
                <!--overview start-->
                
                    <div class="col-lg-12">
                            <h3 class="page-header"><i class="fa fa-laptop"></i>APPLY LEAVE</h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="col-sm-6">
                                        <form method="post" action="controller_leave1.jsp" enctype="multipart/form-data">
                                            <label for="usr">Leave Type:</label>
                                            <div class="radio">

                                                <label><input type="radio" name="leave_type" value="Medical" />Medical</label>
                                            </div>
                                            <div class="radio">

                                                <label><input type="radio" name="leave_type" value="Duty" />Duty Leave</label>
                                            </div>
                                            <div class="radio">

                                                <label><input type="radio" name="leave_type" value="Other" />Others</label>
                                            </div>
                                            <input type="text" name="from_date" class="form-control" placeholder="YYYY-MM-DD" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" title="Enter a date in this formart YYYY-MM-DD"/><br>
                                            <input type="text" name="to_date" class="form-control" placeholder="YYYY-MM-DD" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" title="Enter a date in this formart YYYY-MM-DD"/>
                                            <br> <textarea id="comment" class="form-control" name="comment" row="5" placeholder="Optional comment here.."> </textarea>
                                            <br>
                                            <input type="file" name="file" size="50" />

                                            <input type="submit" name="type" value="Submit Leave" class="btn" style="margin-top: 20px;"/>

                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                
            </section>
        </section>
    <!--main content end-->
    
  
 <%@ include file="footer.jsp" %> 

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
    </body>
</html>
