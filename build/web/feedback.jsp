
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="header.jsp" %>
<%@ include file="sidebar.jsp" %>
<!DOCTYPE html>
<!--main content start-->
        <section id="main-content">
            <section class="wrapper">            
                <!--overview start-->
                
                    <div class="col-lg-12">
                            <h3 class="page-header"><i class="fa fa-laptop"></i>FEEDBACK</h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="col-sm-5 col-sm-offset-1">
                                        <form class="form-validate form-horizontal" id="feedback_form" method="get" action="controller_feedback.jsp">
                                            <div class="form-group">
                                                <label for="sel1">Select class:</label>
                                                <select class="form-control" id="sel1"  name="class">
                                                    <option value="MCA 1">MCA 1</option>
                                                    <option value="MCA 2">MCA 2</option>
                                                    <option value="MCA 3">MCA 3</option>
                                                    <option value="MSC 1">MSC 1</option>
                                                    <option value="MSC 2">MSC 2</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="sel1">Select teacher:</label>
                                                <select class="form-control" id="sel1"  name="teacher">
                                                    <option value="Anuj Kumar">Anuj kumar</option>
                                                    <option value="Ravinder Singla">R.K. Singla</option>
                                                    <option value="Sonal Chawla">Sonal Chawla</option>
                                                    <option value="Anu Gupta">Anu Gupta</option>
                                                    <option value="Syamla Devi">Syamla M. Devi</option>
                                                    <option value="Balwinder Kaur">Balwinder Kaur</option>
                                                    <option value="Indu Chhabra">Indu Chhabra</option>
                                                    <option value="Anuj Sharma">Anuj Sharma</option>
                                                    <option value="Rohini Sharma">Rohini Sharma</option>
                                                    <option value="Jasleen Kaur">Jasleen Kaur</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="comment">Comment:</label>
                                                <textarea class="form-control" rows="5" id="comment" rows="4" column="50" name="comment"></textarea>
                                            </div>
                                            <input type="submit" name="type" value="Submit Feedback" />     
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                
            </section>
        </section>
    <!--main content end-->
 <%@ include file="footer.jsp" %> 

