<%@ include file="header.jsp" %>
<%@ include file="teacher_sidebar.jsp" %>
    <!--main content start-->
        <section id="main-content">
            <section class="wrapper">            
                <!--overview start-->
                
                    <div class="col-lg-12">
                            <h3 class="page-header"><i class="fa fa-laptop"></i>UPLOAD RESOURCES</h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="col-sm-5 col-sm-offset-1">
                                        <form class="form-validate form-horizontal" id="feedback_form" method="post" enctype="multipart/form-data" action="controller_uploadResource.jsp">
                                            
                                            <input type="file" name="file" size="50" class="btn" />

                                            <input type="submit" name="type" value="Upload Resource" class="btn" style="margin-top: 20px;"/></p>     
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                
            </section>
        </section>
    <!--main content end-->
 <%@ include file="footer.jsp" %> 