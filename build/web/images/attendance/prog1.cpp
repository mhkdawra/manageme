//wap of function overloading using friend function : march-31-2016 : Mehak Dawra, Yashda Singla
#include<iostream>
using namespace std;
class areas
{
	int side1,side2;
	public:
	void area(int side1)
	{
		int ar=side1*side1;
		cout<<"area of square "<<ar;
	}
	friend void area(areas);
};
void area(int side1,int side2)
{
	int ar=side1*side2;
	cout<<"area of rectangle "<<ar;
}
int main()
{
	areas ob1,ob2;
	ob1.area(10);
	ob2.area(10,20);
	return 0;
}
