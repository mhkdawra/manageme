//WAP to check whether a number is odd or even
//using class and scope resolution operator
#include<iostream>
using namespace std;
class odev
{
	public:
		void check(int);
};
void odev::check(int a)
{
	if(a%2==0)
	cout<<"The entered number is even."<<endl;
	else
	cout<<"The entered number is odd."<<endl;
}
int main()
{
	odev obj;
	int num;
	cout<<"Enter a number"<<endl;
	cin>>num;
	obj.check(num);
}
