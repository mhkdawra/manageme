<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

<%
  String id = (String)session.getAttribute("userid");

try {
 Class.forName("com.mysql.jdbc.Driver");
} catch (ClassNotFoundException e) {
e.printStackTrace();
}

%>
<%@ include file="header.jsp" %>
<%@ include file="sidebar.jsp" %>
    <!--main content start-->
        <section id="main-content">
            <section class="wrapper">            
                <!--overview start-->
                
                    <div class="col-lg-12">
                            <h3 class="page-header"><i class="fa fa-laptop"></i>DASHBOARD</h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="col-sm-12">
                                        <table class="table table-striped">
                                            <thead>
                                              <tr>
                                                <th>Name</th>
                                                <th>Class</th>
                                                <th>Message</th>
                                                <th>Time</th>
                                              </tr>
                                            </thead>
                                            
                                        <%
                                        try{ 
                                          Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/javadb",
                                                    "root", "");
                                        Statement st = con.createStatement();
                                        ResultSet rs;
                                            rs = st.executeQuery("select * from messages");

                                        while(rs.next()){

                                        %>
                                        <tbody>
                                              <tr>
                                                <td><%=rs.getString("fname") %> <%=rs.getString("lname") %></td>
                                                <td><%=rs.getString("class") %></td>
                                                <td><%=rs.getString("message") %></td>
                                                <td><%=rs.getString("time") %></td>
                                              </tr>
                                            

                                        <% 
                                        }

                                        } catch (Exception e) {
                                        e.printStackTrace();
                                        }
                                        %>
                                        </tbody>
                                        </table>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                
            </section>
        </section>
    <!--main content end-->
 <%@ include file="footer.jsp" %> 