    <%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

<%
  String id = (String)session.getAttribute("userid");

try {
 Class.forName("com.mysql.jdbc.Driver");
} catch (ClassNotFoundException e) {
e.printStackTrace();
}
%>
<%@ include file="header.jsp" %>
<%@ include file="sidebar.jsp" %>
    <!--main content start-->
        <section id="main-content">
            <section class="wrapper">            
                <!--overview start-->
                
                    <div class="col-lg-12">
                            <h3 class="page-header"><i class="fa fa-laptop"></i>DASHBOARD</h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="col-sm-12">
                                        <form class="form-validate form-horizontal" id="info_form" method="get" action="download.jsp">
                                        
                                          <div class="form-group">
                                                <label for="sel1">Select resource:</label>
                                                <select class="form-control" id="sel1" name="c">
                                                    <%try{ 
                                            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/javadb",
                                                      "root", "");
                                          Statement st = con.createStatement();
                                          ResultSet rs;
                                              rs = st.executeQuery("select * from resource");

                                          while(rs.next()){

                                          %>
                                                    <option value="<%=rs.getString("resources") %>"><%=rs.getString("resources") %></option>
                                                    <% 
                                            }

                                            } catch (Exception e) {
                                            e.printStackTrace();
                                            }
                                            %> 
                                                </select>
                                            </div>
                                          <input type="submit" id="thisField" name="inputName" value="Download"><br>
                                            
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                
            </section>
        </section>
    <!--main content end-->
    
  
 <%@ include file="footer.jsp" %> 