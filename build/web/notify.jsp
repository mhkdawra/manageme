<%@ include file="header.jsp" %>
<%@ include file="sidebar.jsp" %>
    <!--main content start-->
        <section id="main-content">
            <section class="wrapper">            
                <!--overview start-->
                
                    <div class="col-lg-12">
                            <h3 class="page-header"><i class="fa fa-laptop"></i>NOTIFICATIONS FOR STUDENTS</h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="col-sm-5 col-sm-offset-1">
                                        <form class="form-validate form-horizontal" id="feedback_form" method="get" action="controller_notify.jsp">
                                            <div class="form-group">
                                                <label for="sel1">Select class:</label>
                                                <select class="form-control" id="sel1" name="c">
                                                    <option value="MCA 1">MCA 1</option>
                                                    <option value="MCA 2">MCA 2</option>
                                                    <option value="MCA 3">MCA 3</option>
                                                    <option value="MSC-IT 1">MSC 1</option>
                                                    <option value="MSC-IT 2">MSC 2</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <textarea class="form-control" rows="5" column="50" name="message" placeholder="Enter Message here.."></textarea>
                                            </div>
                                            <input type="submit" name="type" value="Send Message" /></p>     
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                
            </section>
        </section>
    <!--main content end-->
 <%@ include file="footer.jsp" %> 
