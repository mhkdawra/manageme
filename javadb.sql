-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2017 at 06:09 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `javadb`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(10) NOT NULL,
  `class` varchar(30) NOT NULL,
  `batch` varchar(5) NOT NULL,
  `month` varchar(30) NOT NULL,
  `image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `class`, `batch`, `month`, `image`) VALUES
(1, 'MCA 1', 'M', 'January', 'think.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `t_id` varchar(15) NOT NULL,
  `s_id` varchar(15) NOT NULL,
  `comment` varchar(300) NOT NULL,
  `class` varchar(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`t_id`, `s_id`, `comment`, `class`) VALUES
('Anuj Sharma', 'abcd', 'bkbfkbfksbfkbsjk', ''),
('Indu', '4', 'scasncnc', 'MCA 1'),
('Rohini', '4', 'msc mc', 'MCA 1'),
('Anujj', '4', 'mc smc', 'MCA 2'),
('Anu', '4', 'mc mc', 'MCA 1'),
('Jasleen', '4', 'mcmc ', 'MCA 1'),
('Sonal', '4', 'mc mc', 'MCA 3'),
('Ravinder', '4', ',ss ,c', 'MSC 2');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` varchar(15) DEFAULT NULL,
  `pass` varchar(30) DEFAULT NULL,
  `desig` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `pass`, `desig`) VALUES
('1', 'abc', 'admin'),
(NULL, 'abc', 'Student'),
('3', 'abc', 'Student'),
('anuj', 'sharma', 'Teacher'),
('4', 'abc', 'student'),
('4', 'abc', 'student');

-- --------------------------------------------------------

--
-- Table structure for table `masterdb`
--

CREATE TABLE `masterdb` (
  `id` varchar(15) NOT NULL,
  `desig` varchar(20) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `mname` varchar(20) DEFAULT NULL,
  `lname` varchar(20) DEFAULT NULL,
  `dob` date NOT NULL,
  `gender` varchar(1) NOT NULL,
  `department` varchar(50) DEFAULT NULL,
  `class` varchar(6) NOT NULL,
  `batch` varchar(7) NOT NULL,
  `ph_no` bigint(20) NOT NULL,
  `nationality` varchar(30) DEFAULT NULL,
  `father` varchar(30) DEFAULT NULL,
  `mother` varchar(30) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `bloodgroup` varchar(3) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masterdb`
--

INSERT INTO `masterdb` (`id`, `desig`, `fname`, `mname`, `lname`, `dob`, `gender`, `department`, `class`, `batch`, `ph_no`, `nationality`, `father`, `mother`, `address`, `city`, `state`, `pincode`, `email`, `bloodgroup`) VALUES
('anuj', 'Teacher', 'anuj', '', 'sharma', '1972-08-06', 'M', 'DCSA', 'none', 'none', 9876543210, 'Indian', 'nzc,b', 'BBK', 'VVV', 'VJVV', 'HVJHV', 125212, 'anujs@abc.com', 'A+');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `t_id` varchar(6) NOT NULL,
  `class` varchar(6) NOT NULL,
  `message` varchar(300) NOT NULL,
  `time` varchar(30) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`t_id`, `class`, `message`, `time`, `fname`, `lname`) VALUES
('anuj', 'MCA 1', 'jbjkjk', '2017-04-29 09:52:04', 'anuj', 'sharma'),
('anuj', 'null', 'fjdcjhfjfkfch', '2017-04-29 11:23:47', 'anuj', 'sharma');

-- --------------------------------------------------------

--
-- Table structure for table `timetable`
--

CREATE TABLE `timetable` (
  `id` int(10) NOT NULL,
  `class` varchar(30) NOT NULL,
  `batch` varchar(30) NOT NULL,
  `image` varchar(50) NOT NULL,
  `month` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timetable`
--

INSERT INTO `timetable` (`id`, `class`, `batch`, `image`, `month`) VALUES
(1, 'MCA 1', 'M', 'think.jpg', ''),
(2, 'MCA 2', 'M', 'think.jpg', ''),
(3, 'MCA 3', 'M', 'think.jpg', ''),
(4, 'MCA 1', 'E', 'think.jpg', ''),
(5, 'MCA 2', 'E', 'think.jpg', ''),
(6, 'MCA 3', 'E', 'think.jpg', ''),
(7, 'MSC-IT 1', 'NONE', 'think.jpg', ''),
(8, 'MSC-IT 2', 'NONE', 'think.jpg', ''),
(9, 'null', 'null', 'banner.png', 'null'),
(10, 'null', 'null', 'abc.jpg', 'null'),
(11, 'null', 'null', '61d149cba098e35fc9e403d4979172d4.jpg', 'null'),
(12, 'null', 'null', '3-tier architecture.pdf', 'null'),
(13, 'null', 'null', 'dm_intro.pdf', 'null'),
(14, 'null', 'null', '1.Depression.docx', 'null'),
(15, 'null', 'null', '1.Depression.docx', 'null'),
(16, 'null', 'null', 'assoc-rules1.pptx', 'null'),
(17, 'MCA 1', 'null', 'null', 'null'),
(18, 'MCA 1', 'M', 'null', 'null'),
(19, 'MCA 1', 'M', 'null', 'January'),
(20, 'MCA 1', 'M', 'codifica (1).pdf', 'January'),
(21, 'MCA 1', 'M', 'codifica (1).pdf', 'January'),
(22, 'MCA 1', 'M', 'codifica.pdf', 'January'),
(23, 'MCA 1', 'M', 'codifica (4).pdf', 'January'),
(24, 'MCA 1', 'M', 'codifica (3).pdf', 'January');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`t_id`,`s_id`);

--
-- Indexes for table `masterdb`
--
ALTER TABLE `masterdb`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ph_no` (`ph_no`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`time`);

--
-- Indexes for table `timetable`
--
ALTER TABLE `timetable`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `timetable`
--
ALTER TABLE `timetable`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
